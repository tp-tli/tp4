;(function() {

    function getTotal(price = 15) {
        const total = document.querySelector(`#total`);
        const nbOfGuests = parseInt(document.querySelector(`#guests`).value, 10);
        const priceByGuests = 10;

        total.textContent = price + nbOfGuests * priceByGuests;
    }

    function buildUrl(url) {
        const nbOfNights = parseInt(document.querySelector(`#nights`).value, 10);
        const fetchOpts = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        };
        if (isNaN(nbOfNights)) {
            return fetchOpts;
        }

        fetchOpts.body = `nb=${nbOfNights}`;
        return fetchOpts;
    }

    const newBrowser = (
        `Promise` in window &&
        `fetch` in window
    );

    // I don't want to use XHR anymore, let's focus on the future
    // @see https://fetch.spec.whatwg.org/
    if (!newBrowser) {
        // If fetch doesn't already exist (like in every normal browser) we polyfill it!
        // this element is intended as improvement since adding the polyfill for every
        // user would be a shame for the good users on chrome or firefox
        [`node_modules/whatwg-fetch/fetch.js`, `node_modules/promise-polyfill/promise.min.js`]
            .forEach(e => {
                const scriptEl = document.createElement(`script`);
                scriptEl.src = e;
                scriptEl.async = false;
                document.appendChild(scriptEl);
            })
    }

    function getPrice() {
        const URL = `http://bruno.mascret.fr/tli/ressources/prix.php`;
        const opts = buildUrl(URL);
        return fetch(URL, opts)
            .then((resp) => {
                return resp.text()
            })
            .then((text) => {
                const nightLabel = document.querySelector(`#nights-label`);
                nightLabel.textContent = nightLabel.textContent.replace(`%%`, text);
                const price = parseInt(text, 10);
                getTotal(price);

                document.querySelector(`#guests`).addEventListener(`change`, () => getTotal(price));
            })
            .catch((err) => {
                console.error(`Something bad happened fetching ${URL}`, err);
            });
    }

    function getFact() {
        const URL = `https://api.chucknorris.io/jokes/random`;
        fetch(URL)
            .then(resp => {
                return resp.json();
            })
            .then(json => {
                document.querySelector(`#cnf p`).textContent = json.value;
            })
            .catch(err => console.error(`Chuck norris failed, are you sure ?`, err));
    }

    getPrice();
    getFact();

    document.querySelector(`#nights`).addEventListener(`change`, () => {
        getPrice();
    });

    document.querySelector(`#cnf-btn`).addEventListener(`click`, () => {
        getFact();
    })

})();