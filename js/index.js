'use strict';

;(function () {

    function getTotal() {
        var price = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 15;

        var total = document.querySelector('#total');
        var nbOfGuests = parseInt(document.querySelector('#guests').value, 10);
        var priceByGuests = 10;

        total.textContent = price + nbOfGuests * priceByGuests;
    }

    function buildUrl(url) {
        var nbOfNights = parseInt(document.querySelector('#nights').value, 10);
        var fetchOpts = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        if (isNaN(nbOfNights)) {
            return fetchOpts;
        }

        fetchOpts.body = 'nb=' + nbOfNights;
        return fetchOpts;
    }

    var newBrowser = 'Promise' in window && 'fetch' in window;

    // I don't want to use XHR anymore, let's focus on the future
    // @see https://fetch.spec.whatwg.org/
    if (!newBrowser) {
        // If fetch doesn't already exist (like in every normal browser) we polyfill it!
        // this element is intended as improvement since adding the polyfill for every
        // user would be a shame for the good users on chrome or firefox
        ['node_modules/whatwg-fetch/fetch.js', 'node_modules/promise-polyfill/promise.min.js'].forEach(function (e) {
            var scriptEl = document.createElement('script');
            scriptEl.src = e;
            scriptEl.async = false;
            document.appendChild(scriptEl);
        });
    }

    function getPrice() {
        var URL = 'http://bruno.mascret.fr/tli/ressources/prix.php';
        var opts = buildUrl(URL);
        return fetch(URL, opts).then(function (resp) {
            return resp.text();
        }).then(function (text) {
            var nightLabel = document.querySelector('#nights-label');
            nightLabel.textContent = nightLabel.textContent.replace('%%', text);
            var price = parseInt(text, 10);
            getTotal(price);

            document.querySelector('#guests').addEventListener('change', function () {
                return getTotal(price);
            });
        }).catch(function (err) {
            console.error('Something bad happened fetching ' + URL, err);
        });
    }

    function getFact() {
        var URL = 'https://api.chucknorris.io/jokes/random';
        fetch(URL).then(function (resp) {
            return resp.json();
        }).then(function (json) {
            document.querySelector('#cnf p').textContent = json.value;
        }).catch(function (err) {
            return console.error('Chuck norris failed, are you sure ?', err);
        });
    }

    getPrice();
    getFact();

    document.querySelector('#nights').addEventListener('change', function () {
        getPrice();
    });

    document.querySelector('#cnf-btn').addEventListener('click', function () {
        getFact();
    });
})();