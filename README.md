# TP4 - TLI CPE

[[ ← ONLINE → ]](http://tp-tli.gitlab.io/tp4)

Ce TP vise à introduire l'utilisation d'Ajax. 
On a utilisé la dernière spécification [`fetch`](https://fetch.spec.whatwg.org/), plutôt que `XHR`,
pour correspondre à un développement moderne en javascript.
De même on a utilisé la syntaxe `es6`, pour ajouter améliorer la lisibilité du code et profiter
du sucre syntaxique proposé par cette spécification
([implémentée dans les navigateurs récent](https://kangax.github.io/compat-table/es6/)).

Dans le cas où le navigateur où s'éxécute le script n'implémente pas ces éléments, on a utilisé 
des polyfills. Le script en lui-même est compilé avec [babel](http://babeljs.io/).

## Liste des polyfills utilisés

+ [whatwg-fetch](https://npmjs.com/whatwg-fetch)
+ [promise-polyfill](https://npmjs.com/promise-polyfill) (Pour implémenter la spécification
  [Promise A+](https://promisesaplus.com/))

## Scripts

### Installation des dépendances

Avant toute chose il est nécessaire de résoudre les dépendances du projet.
Pour ce faire l'utilisation de [npm](https://npmjs.com) est recommandée. 

    $ npm install --production

Cette commande crée un dossier `node_modules` dans lequel est placé l'ensemble des dépendances
résolues.

### Serveur de développement web

Lorsque l'on veut effectuer des appels Ajax il est souvent recommandé d'utiliser un serveur web
plutôt qu'en faisant un appel en `file://...`.
Toutefois l'installation d'un serveur web peut être contraignante, on privilégiera l'utilisation
d'un serveur portable. 
Par défaut python embarque un serveur web portable. Python est installé de base sur la majorité des
systèmes Unix. On peut donc l'utiliser.

Pour lancer un serveur de développement (utiliser firefox de préférence), on peut utiliser la
commande suivant qui lance un serveur web python:

    $ npm start

Cette commande se contente de lancer la commande suivante

    $ python -m SimpleHTTPServer

Il est possible de configurer différents paramètres pour ce serveur (port, dossier à service, ...),
il faudra lire la doc pour aller plus loin, mais cette commande suffit amplement pour l'utilisation
que nous en faisons.

### Compilation d'es6 vers es5

**ATTENTION**: Pour utiliser cette fonctionnalité il est nécessaire que les dépendances de
développement soient installées, si vous avez utilisé `--production` avec npm pour l'installation
des dépendances il va être nécessaire de relance un simple

    $ npm install

On utilise le projet [babel](https://babeljs.io) pour compiler (transpiler) les sources es6 vers es5
qui sera par définition plus accessible qu'es6 qui ne fonctionne que sur les navigateurs récent. 
D'aucun dirait que la norme serait de les utiliser, mais la question n'est pas là.

Pour générer un fichier qui soit portable d'un navigateur à l'autre on peut utiliser la commande
suivante

    $ npm run build

Cette commande utilise l'utilitaire `babel-cli` pour transpiler le fichier javascript dans le
dossier `src/` en `es6` vers le dossier `js/` en `es5`. 
Pour le développement, on peut utiliser la commande:

    $ npm run build:dev

qui va se charger de transpiler le fichier `es6` vers `es5` au changement de ce dernier. 

Plus simplement on peut directement utiliser le ficheir `es6` dans un navigateur récent,
en changeant la ligne dans le fichier `index.html` (préféré)

```diff
-   <script src="js/index.js"></script
+   <script src="src/index.js></script>
```
